require('dotenv').config()

const Obserser = require("./observer");
const obserser = new Obserser();

const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const client = require('twilio')(accountSid, authToken);

const FILE = process.env.LOG;
const WAIT_PERIOD = 900000; // 15 minutes
let lastText = 1608750977425; // some timestamp :)

obserser.on("file-updated", (log) => {
  const service = log.service;
  const message = log.message;

  switch (service) {
    case "Nest":
      nest(message);
      break;
    case "myQ":
      myq(message);
      break;
    case "Arlo":
      arlo(message);
      break;
    default:
      console.log(`${service} did something :)`)
      break;
  }
});

obserser.watchFile(FILE);

function myq(message) {
  switch (message) {
    case "myQ API: Unable to update device status from myQ servers. Acquiring a new security token and retrying later.":
      console.log("MYQ ERROR 1");
      twilio(message);
      break;
    case "myQ API: Unable to authenticate. Will retry later.":
      console.log("MYQ ERROR 2");
      twilio(message);
      break;
    default:
      break;
  }
  return;
}

function nest(message) {
  switch(message){
    case "API observe: error Error: read ECONNRESETat TLSWrap.onStreamRead (internal/stream_base_commons.js:205:27) {errno: 'ECONNRESET',code: 'ECONNRESET',syscall: 'read'}":
      console.log('NEST ERROR 1')
      twilio(message);
      break;
    default:
      break;
  }
  return;
}

function arlo(message) {
  return;
}

function twilio(message){
  const textTime = Date.now();
  if(textTime - lastText > WAIT_PERIOD){
    // update the text time
    lastText = textTime;
    return client.messages
      .create({
          body: message,
          from: process.env.FROM,
          to: process.env.TO
        })
    .then(message => console.log(message.sid));
  } else {
    return
  }
}
