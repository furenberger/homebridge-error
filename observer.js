const chokidar = require("chokidar");
const EventEmitter = require("events").EventEmitter;
const fs = require("fs");
const stripAnsi = require("strip-ansi");

class Observer extends EventEmitter {
  constructor() {
    super();
  }

  watchFile(targetFile) {
    try {
      console.log(
        `[${new Date().toLocaleString()}] Watching for file changes on: ${targetFile}`
      );

      const watcher = chokidar.watch(targetFile, { persistent: true });

      watcher.on("change", (filePath) => {
        // console.log(
        //   `[${new Date().toLocaleString()}] ${filePath} has been updated.`
        // );

        const { message, service } = getServiceAndMessageFromLog(filePath);

        // emit an event when the file has been updated
        this.emit("file-updated", { message, service });
      });
    } catch (error) {
      console.log(error);
    }
  }
}

/**
 Lines will look like this:
  [12/23/2020, 10:36:04] [Nest] Authentication successful.
  [12/22/2020, 17:27:05] [myQ] myQ API: Unable to update device status from myQ servers. Acquiring a new security token and retrying later.
  [12/22/2020, 18:03:09] [myQ] myQ API: Unable to authenticate. Will retry later.
  [12/23/2020, 09:15:37] [Nest] API observe: error Error: read ECONNRESET
    at TLSWrap.onStreamRead (internal/stream_base_commons.js:205:27) {
    errno: 'ECONNRESET',
    code: 'ECONNRESET',
    syscall: 'read'
  }

  So we will read one line at a time, starting from the bottom until we get a [time] [service] [message] format
 **/
const getServiceAndMessageFromLog = (filePath) => {
  const raw = fs.readFileSync(filePath, "utf-8");
  const lines = raw.split("\n");

  let linesLength = lines.length - 1;
  let line = extractLine(lines[linesLength]);

  if (line.length < 3) {
    message = line[0];

    while (line.length < 3) {
      linesLength -= 1;
      line = extractLine(lines[linesLength]);

      service = line[1];
      if (!service) {
        message = line[0] + message;
      } else {
        message = line[2] + message;
      }
    }
  } else {
    message = line[2];
  }

  return { service, message };
};

module.exports = Observer;

const extractLine = (ansiLine) => {
  const line = stripAnsi(ansiLine);
  // .replace(/\x1B\[([0-9]{1,3}(;[0-9]{1,2})?)?[mGK]/g, "")

  return line.split("]").map((data) => {
    return data.replace(" [", "").replace("[", "").trim();
  });
};
