`/etc/systemd/system/homebridge-error.service`

```bash
[Unit]
Description=Homebridge-Error
Wants=network-online.target
After=syslog.target network-online.target

[Service]
Type=Simple
User=pi
PermissionsStartOnly=true
WorkingDirectory=/home/pi/homebridge-error
ExecStart=/usr/local/bin/node --expose-gc /home/pi/homebridge-error/index.js
Restart=always
RestartSec=3
SyslogIdentifier=homebridge-error
KillMode=process

[Install]
WantedBy=multi-user.target
```


```
$ sudo systemctl enable homebridge-error
$ sudo systemctl start homebridge-error
```